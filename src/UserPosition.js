import React from 'react';
import getVenuesForPosition from './FetchVenueData';


const UserPosition = props => {
  const { setVenueState } = props;
  // here if the app were being internationalised I would have the "Find food near me" given an id or stored somewhere separately
  return (
    <div>
      <p>Find open food venues near your location</p>
      <button
        className="forage-button"
        type="button"
        onClick={getVenuesForPosition(setVenueState)}
        id='app.find_food-button'>
        Find food near me
    </button>
    </div>
  );
}

export default UserPosition;