import React from 'react';
import Loading from './Loading';

// In a prodction app I would either check that these properties existed or make sure I was passing in an object where they were guarenteed to exist
const Venue = ({ venue }) => (
  <li className='venue-list__venue'>
    <h3>{venue.name}</h3>
    <p>{`location: ${venue.location.address}`}</p>
    {venue.categories[0] == null ? null : <p>{`category: ${venue.categories[0].name}`}</p>}
    <p>{`estimated distance from you: ${venue.location.distance}m`}</p>
    <p><a href={`http://maps.google.com/?q=${venue.location.lat},${venue.location.lng}`}>Show on map</a></p>
  </li>
);

const VenueList = ({ venues }) => (
  <ul className='venue-list'>
    {
      venues.map(venue => {
        return <Venue venue={venue} key={venue.id} />
      })
    }
  </ul>
);

const VenueContainer = ({ venues }) => {
  // this only shows the user loading states but I would have notifications of what went wrong and what the user could do in those situations
  // I would also retry with polly-js if a call failed and I would a backoff strategy so I didn't send too much traffic in case the servers went down
  if (venues == null) {
    return <Loading />;
  }

  return (venues.length
    ? <VenueList venues={venues} />
    : 'No venues');
};

export default VenueContainer;