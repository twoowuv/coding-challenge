/**
 * Creates a promise to get the users location from the geolocation api. set to timeout after 9 seconds
 */
export default function createGeolocationPromise() {

  // In production, I would use polly-js or something to retry with some sort of exponential backoff
  return new Promise((resolve, reject) => {
    navigator.geolocation.getCurrentPosition(
      position => resolve(position),
      error => reject(error),
      {
        timeout: 9000,
        enableHighAccuracy: true
      }
    );
  });
}
