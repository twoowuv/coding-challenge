import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import createVenueSearchPromise from './FetchVenueData';
import Assert from 'assert';

// I would wrap these in describe blocks to group them so they were easier to look through when there are lots of tests
// Also I would write a lot more tests for a production app probably using jest
it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<App />, div);
  ReactDOM.unmountComponentAtNode(div);
});

it('throws an error when latitude is not passed', () => {
  Assert.throws(function () {
    createVenueSearchPromise({ longitude: '42.3' });
  });
});

it('throws an error when longitude is not passed', () => {
  Assert.throws(function () {
    createVenueSearchPromise({ latitude: '42.3' });
  });
});

it('returns a promise when an object with latitude and longitude are passed', () => {
  const venuePromise = createVenueSearchPromise({ longitude: '82.3', latitude: '42.3' });

  Assert.strictEqual((venuePromise instanceof Promise), true);
});
