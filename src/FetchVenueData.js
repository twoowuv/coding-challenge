import uxhr from 'uxhr';
import createGeolocationPromise from './FetchGeolocationData';

// Obviously you wouldn't do this in production.
// Instead I would create a session and CSRF secured ajax call to my backend and let them make the call to foursquare for me
// This means the keys aren't exposed unless I somehow return them to the frontend or the backend is compromised.
// This is highly secure but means you have to run more traffic through your backend so if you're dealing with higher volumes
// of data you might consider asking your backend for a single session token to authenticate with the foursquare api or something similar.
// Or if you don't mind making your users log into something you could use OAuth.
const CLIENT_SECRET = '0KUB1WOIHJGR2GKG14RHDSNTRGYAOMEJV51YJ0UJIWCVH1JS';
const CLIENT_ID = 'BYWOQ2QVLIS2QNFDOB410METPTEJQK4TGFHJP3S1JQ4EREEL';
const VERSION = '20190221';


// Again I would use redux for a production app and this would be in an action that called the geolocation api
// and would return a fetching_location, location_success and location_failure. also possibly a reset_location_data
export default function getVenuesForPosition(setVenueStateFn) {
  return function () {
    // clear old data. null is currently being used in place of an isLoading model state which I would do to make things easier to understand.
    setVenueStateFn(null);
    const locationPromise = createGeolocationPromise();

    locationPromise.then(
      position => createVenueSearchPromise(position.coords),
      // this would go to logging and retry and I would also inform the user of what is happening
      error => { console.log('failed to get user location: ', error); }
    ).then(
      data => {
        setVenueStateFn(data.response.venues);
        return data;
      },
      // this would go to logging and retry and I would also inform the user of what is happening
      error => { console.log('failed to get venues: ', error); }
    );
  }
}
/**
 * Takes a coords object and returns a promise of data from the foursquare api
 *
 * @param {object} position
 * @param {string} position.latitude
 * @param {string} position.longitude
 */
function createVenueSearchPromise({ latitude, longitude }) {

  if (!latitude) {
    throw new Error('Missing latitude information');
  }

  if (!longitude) {
    throw new Error('Missing longitude information');
  }

  return new Promise((resolve, reject) => {
    // I have hard coded the things I want to look for here (food that is open now). For a real app I would allow the user to have some choice in this
    // and I would also let them sort the information by distance or price.
    uxhr('https://api.foursquare.com/v2/venues/search',
      {
        ll: `${latitude},${longitude}`,
        section: 'food',
        openNow: 1,
        client_id: CLIENT_ID,
        client_secret: CLIENT_SECRET,
        v: VERSION
      },
      {
        success: function (response) {
          try {
            const data = JSON.parse(response);
            resolve(data);
          } catch (error) {
            reject(error);
          }
        },
        error: function (error) {
          reject(error);
        }
      }
    );
  });
}