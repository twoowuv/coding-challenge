import React, { Component } from 'react';
import VenueContainer from './VenueContainer';
import UserPosition from './UserPosition';

import './App.css';
// If I were building a production app I would definitely use react-redux.
// That would also mean some checks like this would be moved to an action so I could make this
// react part focused on the view and I think clearer.

const geolocationIsAvailable = ("geolocation" in navigator);

export default class App extends Component {
  constructor(props) {
    super(props);
    // this would ideally be the initial data from redux instead of state here
    this.state = {
      venues: []
    };
    this.setVenueState = this.setVenueState.bind(this);
  }

  // I'm passing this function down to allow child components to set this components state because I haven't used redux here
  setVenueState(venues) {
    const forage = this;
    forage.setState(() => ({ venues }));
  }

  render() {
    const state = this.state;
    return (
      <div className="App">
        <header>
          <h1>Forage</h1>
        </header>
        {geolocationIsAvailable
          ? <UserPosition setVenueState={this.setVenueState} />
          : 'geolocation isn\'t available for this browser. you can manually enter your lat and long here: TODO'}
        <VenueContainer venues={state.venues} />
      </div>
    );
  }
}
